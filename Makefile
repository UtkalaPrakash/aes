hw5: hw5.o
	gcc -o hw5 -g hw5.o -lcrypto

hw5.o: hw5.c
	gcc -g -c -Wall hw5.c -I/usr/local/opt/openssl/include

clean:
	rm -f *.o hw5
