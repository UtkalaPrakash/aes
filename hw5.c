#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/types.h>
#include <openssl/md5.h>
#include <getopt.h>
#include <math.h>

unsigned int prod[4], inv[4], A[4], A_inv[4], S[16][16], INV[256], S_inv[16][16];
unsigned int Rcon[10] = {0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36};
unsigned long int keys[44] = {0}, shift[4] = {0};
void pusage(char *string) {
	// This prints the usage and ends the program on occurrence of error.
	fprintf(stderr, "%s\n", string);
	fprintf(stderr, "Usage:\n");
	fprintf(stderr, "\t./hw5 tablecheck -t=tablefile\n");
	fprintf(stderr, "\t./hw5 modprod -p1=poly1 -p2=poly2\n");
	fprintf(stderr, "\t./hw5 keyexpand -k=key -t=tablefile\n");
	fprintf(stderr, "\t./hw5 encrypt -k=key -t=tablefile [file]\n");
	fprintf(stderr, "\t./hw5 decrypt -k=key -t=tablefile [file]\n");
	fprintf(stderr, "\t./hw5 inverse -p=poly\n");
	exit(1);
}

FILE* fileCheck(char *file_name) {
	// This function checks whether input file is readable and its existence.
	if (strlen(file_name) == 0) pusage("Malformed command");
	if ( access(file_name, F_OK) == -1 ) {
	    fprintf(stderr, "Input file %s doesn't exist\n", file_name);
	    exit(1);
	}

	struct stat file_check;
	stat(file_name, &file_check);
	if ( S_ISDIR(file_check.st_mode) ) {
	    fprintf(stderr, "Input file %s is a directory\n", file_name);
	    exit(1);
	}

	if ( access(file_name, R_OK) == -1 ) {
	    fprintf(stderr, "Cannot read input file %s, permission denied\n", file_name);
	    exit(1);
	}

	FILE *file_handler = fopen(file_name, "r");
	if ( file_handler == NULL) {
            fprintf(stderr, "Input file %s cannot be open\n", file_name);
            exit(1);
        }
	return file_handler;
}

void print16(unsigned long int input[4]) {
	// This prints 16 bytes of array as string
	int i;
	for(i=0;i<4;i++) printf("%08lx", input[i]);
	printf("\n");
}

int hex(char *string) {
	// This converts hex string to hex number
	int num = 0, i;
	for (i=1; i>=0; i--) {
		if ( *string >= 97 && *string <= 102 ) {
			num += (*string - 87) * pow(0x10,i);
		} else if ( *string >= 48 && *string <= 57 ) {
			num += (*string - 48) * pow(0x10,i);
		} else {
			pusage("Invalid Hex character in input");
		}
		string++;
	}
	return(num);
}

unsigned long int hex_string(unsigned int input[4]) {
	// converts input array to string
	unsigned long int string = 0;
	int i;
	for (i=0; i<4; i++) string += input[i] * pow(2, (8*(3-i)));
	return string;
}

long int xor(long int a, long int b, int size) {
	// This function XOR 2 integers bitwise
	int i =0;
	unsigned long long int c = 0;
	for (i=0; i< size; i++) c += (((a >> i) & 0x1) ^ ((b >> i) & 0x01)) * pow(2, i);
	return c;
}

int big_dot(int a, int b) {
	// This performs 1 byte big dot operation
	int i, temp[8], sum = 0;
	temp[0] = b;
	for (i=0; i<7; i++) {
		int bit = (b & 0x80) >> 7;
		if (bit == 1) {
			b = (b*2) & 0xff;
			b = (int) xor((long int) b, (long int) 0x1b, 8);
		} else {
			b = (b*2) & 0xff;
		}
		temp[i+1] = b;
	}

	for (i=7; i>=0; i--) {
		int bit = (a >> i) & 0x01;
		if (bit == 1) {
			sum = (int) xor((long int) sum, (long int) temp[i], 8);
		}
	}
	return sum;
}

void modprod(unsigned int x[4], unsigned int b[4]) {
	// This does modular multiplication in GF8
	int i, j;
	int a[4][4] = {{x[0], x[3], x[2], x[1]}, {x[1], x[0], x[3], x[2]}, {x[2], x[1], x[0], x[3]}, {x[3], x[2], x[1], x[0]}};
	for (i=0; i<4; i++) {
		int sum = 0;
		for (j=0; j<4; j++) {
			int temp = big_dot(a[i][j], b[j]);
			sum = (int) xor((long int) sum, (long int) temp, 8);
		}
		prod[i] = sum;
	}
}

void inverse() {
	// Calculates the inverse of all numbers from 0 to 255 and stores in an global array INV
	int i=1, j;
	INV[0] = 0;
	for(;i<256; i++) {
		for (j=1; j<256; j++) {
			if (big_dot(j, i) == 1) INV[i] = j;
		}
	}
}

void inverse_gf8(unsigned int inp[4]) {
	//Calculates the inverse of GF8 polynomial
	int i, ka = 4, kb = 5, count=3;
	unsigned int rem[2][5] = {{0},{0}}, x[2][4] = {{0}, {0}};
	x[1][0] = 1;
	rem[0][4] = 1;
	rem[0][0] = 1;
	for (i=0; i<5; i++) rem[1][i] = inp[i];
	printf("i=1, rem[i]={00}{00}{00}{01}, quo[i]={00}{00}{00}{00}, aux[i]={00}{00}{00}{00}\n");
	printf("i=2, rem[i]={%02x}{%02x}{%02x}{%02x}, quo[i]={00}{00}{00}{00}, aux[i]={00}{00}{00}{01}\n", inp[3], inp[2], inp[1], inp[0]);
	for (; ka>=0; ka--) {
		// Longhand division b/a
		int b[5] = {0}, a[4] = {0};
		unsigned int quo[5] = {0};
		for (i=4; i>=0; i--) {
			b[i] = rem[0][i];
			if (i != 4) a[i] = rem[1][i];
		}

		// Size of b
		for(i=4; i>=0; i--) {
			if (b[i] != 0) {
				kb = i;
				break;
			}
		}

		// Size of a
		for(i=3; i>=0; i--) {
			if (a[i] != 0) {
				ka = i;
				break;
			}
		}
		for (i=kb; i>=ka; i--) {
			if (b[i] == 0) {
				quo[i-ka] = 0;
				continue;
			}
			int temp;
			if (ka==0 && i==0) {
				quo[i-ka] = big_dot(INV[a[ka]], xor(b[i], 0x01, 8));
			} else {
				quo[i-ka] = big_dot(INV[a[ka]], b[i]);
			}
			int k = i, j;
			for (j=ka; j>=0; j--) {
				temp = big_dot(a[j], quo[i-ka]);
				b[k] = (int) xor(b[k], temp, 8);
				k--;
			}
		}
		printf("i=%d, rem[i]=", count);
		for (i=3; i>=0; i--) {
			printf("{%02x}", b[i]);
			rem[0][i] = rem[1][i];
			rem[1][i] = b[i];
		}
		rem[0][4] = 0;
		rem[1][4] = 0;
		printf(", quo[i]={%02x}{%02x}{%02x}{%02x}", quo[3], quo[2], quo[1], quo[0]);
		printf(", aux[i]=");
		modprod(quo, x[1]);
		for (i=3; i>=0; i--) {
			int temp = (int) xor(prod[i], x[0][i], 8);
			x[0][i] = x[1][i];
			x[1][i] = temp;
			printf("{%02x}", temp);
		}
		printf("\n");
		count++;
	}
	for (i=0; i<4; i++) inv[i] = x[1][i];
}

int hex_character(char c) {
	if ( (c >= 48 && c <= 57) || (c >= 97 && c <= 102) ) {
		return 1;
	} else {
		return 0;
	}
}

void table_check(FILE *fh) {
	// Verifies the input file and prepares global permutation tables
	int k;
	for (k=0; k<3; k++) {
		char input[3] = "";
		fread(input, 2, 1, fh);
		if (input[0] == 'S' && input[1] == '=') {
			int i=0, exist[256] = {0};
			for(;i<256;i++) {
				fread(input, 2, 1, fh);
				if ( !hex_character(input[0]) || !hex_character(input[1]) ) {
					fprintf(stderr, "Malformed values at line %d\n", k+1);
					exit(1);
				}
				int val = hex(input);
				if (exist[val]) {
					fprintf(stderr, "Duplicate value %02x at line %d\n", val, k+1);
					exit(1);
				}
				exist[val]++;
				S[i/16][i%16] = val;
				short int x = (val & 0xf0) >> 4;
				short int y = (val & 0x0f);
				S_inv[x][y] = ((i/16) * 0x10) + ((i%16) * 0x1);
			}
		} else if (input[0] == 'P' && input[1] == '=') {
			int i=0;
			for (;i<4; i++) {
				fread(input, 2, 1, fh);
				if ( !hex_character(input[0]) || !hex_character(input[1]) ) {
					fprintf(stderr, "Malformed values at line %d\n", k+1);
					exit(1);
				}
				A[3-i] = hex(input);
			}
		} else if (input[0] == 'I' && input[1] == 'N') {
			fread(input, 2, 1, fh);
			if (input[0] != 'V' || input[1] != 'P') {
				fprintf(stderr, "Malformed table input at line %d\n", k+1);
				exit(1);
			}
			fread(input, 1, 1, fh);
			int i=0;
			for (;i<4; i++) {
				fread(input, 2, 1, fh);
				if ( !hex_character(input[0]) || !hex_character(input[1]) ) {
					fprintf(stderr, "Malformed values at line %d\n", k+1);
					exit(1);
				}
				A_inv[3-i] = hex(input);
			}
 		} else {
 			if ( hex_character(input[0]) && hex_character(input[1]) ) {
				fprintf(stderr, "Malformed values at line %d\n", k);
				exit(1);
			} else {
				fprintf(stderr, "Malformed table input at line %d\n", k+1);
				exit(1);
			}
		}
		fseeko(fh, 1, SEEK_CUR);
	}
} 

unsigned int SubBytes (unsigned int input) {
	// Does byte wise substitution according to S box
	short int x = (input & 0xf0) >> 4;
	short int y = (input & 0x0f);
	return(S[x][y]);
}

unsigned int InvSubBytes (unsigned int input) {
	// Does byte wise substitution according to Inverse S box
	short int x = (input & 0xf0) >> 4;
	short int y = (input & 0x0f);
	return(S_inv[x][y]);
}

unsigned long int RotWord(unsigned long int input) {
	// Rotates word in anti clockwise direction
	input = ((input & 0xffffff) * 0x100) + ((input >> 24) & 0xff);
	return input;
}

unsigned long int AntiRotWord(unsigned long int input) {
	// Rotates word in clockwise direction
	input = ((input >> 8) & 0xffffff) + ((input & 0xff) * pow(2, 24));
	return input;
}

unsigned long int SubWord(unsigned long int input) {
	// Substitutes complete word using Sbytes
	unsigned long int output = 0;
	int i =0;
	for (;i<4; i++) {
		int temp = (input >> (i*8)) & 0xff;
		temp = SubBytes(temp);
		output += temp * pow(2, i*8);
	}
	return output;
}

unsigned long int InvSubWord(unsigned long int input) {
	// Inverse substitutes complete word using Inverse Sbox
	unsigned long int output = 0;
	int i =0;
	for (;i<4; i++) {
		int temp = (input >> (i*8)) & 0xff;
		temp = InvSubBytes(temp);
		output += temp * pow(2, i*8);
	}
	return output;
}

void ShiftRows (unsigned long int input[4], short int inv) {
	// Shifts rows according to their row index
	int i, j;
	for (i=0; i<4; i++) shift[i] = 0;
	for (i=0; i<4; i++) {
		for (j=3; j>=0; j--) {
			shift[3-j] += ((input[i] >> (j*8)) & 0xff) << (3-i)*8;
		}
	}
	for (i=0; i<4; i++) {
		input[i] = shift[i];
		for (j=0; j<i; j++) {
			shift[i] = (inv) ? AntiRotWord(shift[i]): RotWord(shift[i]);
		}
		input[i] = shift[i];
		shift[i] = 0;
	}
	for (i=0; i<4; i++) {
		for (j=3; j>=0; j--) {
			shift[i] += ((input[3-j] >> ((3-i)*8)) & 0xff) << j*8;
		}
	}
}

void keyexpansion(unsigned int key[16]) {
	//Expands 16 bytes key to 44 bytes key
	int i, Nk = 4, Nr = 10, Nb = 4;
	for (i=0; i < Nk; i++) {
		keys[i] = ((key[4*i] << 24) + (key[4*i+1] << 16) + (key[4*i+2] << 8)+ key[4*i+3]) & 0xffffffff;
	}
	for (i=Nk; i < Nb * (Nr+1); i++) {
		unsigned long int temp = keys[i-1];
		if (i % Nk == 0) {
			unsigned long int xtemp = SubWord(RotWord(temp));
			temp = xor(xtemp, (long int) (Rcon[(i/Nk)-1] * 0x1000000), 32);
		}
		keys[i] = xor(keys[i-Nk], temp, 32);
	}
}

void encrypt_fun (unsigned long int input[4], int print) {
	// AES encryption algorithm. Prints only for first 16 bytes encryption
	int i, round;
	if (print) {
		printf("round[ 0].input    ");
		print16(input);
		printf("round[ 0].k_sch    ");
	}
	for (i=0; i<4; i++) {
		if (print) printf("%08lx", keys[i]);
		input[i] = xor(input[i], keys[i], 32);
	}
	if (print) printf("\n");
	for (round = 0; round < 10; round++) {
		if (print) {
			printf("round[%2d].start    ", round+1);
			print16(input);
		}
		for(i=0; i<4; i++) input[i] = SubWord(input[i]);
		if (print) {
			printf("round[%2d].s_box    ", round+1);
			print16(input);
		}
		ShiftRows(input, 0);
		if (print) {
			printf("round[%2d].s_row    ", round+1);
			print16(shift);
		}
		if (round != 9) {
			for(i=0; i<4; i++) {
				unsigned int temp[4];
				int j;
				for (j=0; j<4; j++) temp[j] = (shift[i] >> ((3-j)*8)) & 0xff;
				modprod(A, temp);
				input[i] = hex_string(prod);
			}
			if (print) {
				printf("round[%2d].m_col    ", round+1);
				print16(input);
			}
			if (print) printf("round[%2d].k_sch    ", round+1);
			for (i=0; i<4; i++) {
				if (print) printf("%08lx", keys[(round+1)*4 + i]);
				input[i] = xor(input[i], keys[(round+1)*4 + i], 32);
			}
			if (print) printf("\n");
		}
	}
	if (print) printf("round[10].k_sch    ");
	for(i=0; i<4; i++) {
		if (print) printf("%08lx", keys[(round*4) + i]);
		input[i] = xor(shift[i], keys[(round*4) + i], 32);
	}
	if (print) {
		printf("\nround[10].output   ");
		print16(input);
	}
}

void decrypt_fun (unsigned long int input[4], int print) {
	// AES decryption algorithm. Prints only for first 16 bytes decryption
	int i, round;
	if (print) {
		printf("round[ 0].iinput   ");
		print16(input);
		printf("round[ 0].ik_sch   ");
	}
	for (i=0; i<4; i++) {
		if (print) printf("%08lx", keys[40+i]);
		input[i] = xor(input[i], keys[40+i], 32);
	}
	if (print) printf("\n");
	for (round = 0; round < 10; round++) {
		if (print) {
			printf("round[%2d].istart   ", round+1);
			print16(input);
		}
		ShiftRows(input, 1);
		if (print) {
			printf("round[%2d].is_row   ", round+1);
			print16(shift);
		}
		for(i=0; i<4; i++) input[i] = InvSubWord(shift[i]);
		if (print) {
			printf("round[%2d].is_box   ", round+1);
			print16(input);
			printf("round[%2d].ik_sch   ", round+1);
		}
		for (i=0; i<4; i++) {
			if (print) printf("%08lx", keys[(9-round)*4 + i]);
			input[i] = xor(input[i], keys[(9-round)*4 + i], 32);
		}
		if (round != 9) {
			if (print) {
				printf("\nround[%2d].ik_add   ", round+1); 
				print16(input);
			}
			for(i=0; i<4; i++) {
				unsigned int temp[4];
				int j;
				for (j=0; j<4; j++) temp[j] = (input[i] >> ((3-j)*8)) & 0xff;
				modprod(A_inv, temp);
				input[i] = hex_string(prod);
			}
		}	
	}
	if (print) {
		printf("\nround[10].ioutput  ");
		print16(input);
	}
}

int main (int argc, char **argv) {
	if (argc < 3) pusage("Malformed command");
	char tablefile[50] = "", option[15] = "";
	unsigned int key[16], p1[4] = {0}, p2[4] = {0};
	if (	strcmp(*(argv+1), "tablecheck") != 0 && 
			strcmp(*(argv+1), "encrypt") != 0 && 
			strcmp(*(argv+1), "decrypt") != 0 && 
			strcmp(*(argv+1), "modprod") != 0 && 
			strcmp(*(argv+1), "keyexpand") != 0 &&
			strcmp(*(argv+1), "inverse") != 0) {
	    pusage("Malformed command");
    } else {
	    strcpy(option, *(argv+1));
	}

	// Handle input arguments
	int index = 0, opt;
	static struct option options[] = {
        	{"k",    required_argument, 0,  'k' },
        	{"t",    required_argument, 0,  't' },
        	{"p",    required_argument, 0,  'p' },
        	{"p1",    required_argument, 0,  'x' },
        	{"p2",    required_argument, 0,  'y' },
			{NULL, 0, NULL, 0}
	};

	while ((opt = getopt_long_only(argc, argv,"tkp:p1:p2", options, &index)) != -1) {
		int i;
		if ( opt == 't' ) {
		    sprintf(tablefile, "%s", optarg);
		} else if ( opt == 'k' ) {
			if (strlen(optarg) != 32) pusage("Bad key, Not enough characters");
			for (i=0; i<strlen(optarg); i+=2) key[i/2] = hex(optarg+i);
		} else if ( opt == 'p' ) {
			if (strlen(optarg) != 8) pusage("Invalid input polynomial");
			for (i=0; i<strlen(optarg); i+=2) A_inv[3-i/2] = hex(optarg+i);
		} else if ( opt == 'x' ) {
			if (strlen(optarg) != 8) pusage("Invalid input P1 polynomial");
			for (i=0; i<strlen(optarg); i+=2) p1[3-i/2] = hex(optarg+i);
		} else if (opt == 'y' ) {
			if (strlen(optarg) != 8) pusage("Invalid input P2 polynomial");
			for (i=0; i<strlen(optarg); i+=2) p2[3-i/2] = hex(optarg+i);
		} else {
			pusage("Malformed command");
		}
    }

    if ( strcmp(option, "tablecheck") == 0) {
    	inverse();
    	FILE *fTable = fileCheck(tablefile);
    	table_check(fTable);
    } else if ( strcmp(option, "encrypt") == 0 || strcmp(option, "decrypt") == 0 ) {
    	FILE *fTable = fileCheck(tablefile);
    	table_check(fTable);
    	FILE *file_handler;
    	if (argc == 5) {
    		file_handler = fileCheck(*(argv+4));
    	} else {
    		file_handler = stdin;
    	}
    	inverse();
    	keyexpansion(key);
    	int first = 1;
    	while (!feof(file_handler)) {
    		int end = 0, i, j;
    		unsigned long int inp[4] = {0};
    		for (i=0; i<4; i++) {
    			for (j=0; j<4; j++) {
    				unsigned char input;
    				fread(&input, 1, 1, file_handler);
    				if (feof(file_handler)) {
    					end = 1;
    					break;
    				}
    				unsigned long int ascii = ((int) input) * pow(2, ((3-j)*8));
    				inp[i] += ascii;
    			}
    		}
    		(strcmp(option, "encrypt") == 0) ? encrypt_fun(inp, first) : decrypt_fun(inp, first);
    		first = 0;
    		if (end == 1) break;
    	}
    } else if ( strcmp(option, "modprod") == 0) {
    	if (argc != 4) pusage("Malformed command");
    	int i;
    	modprod(p1, p2);
    	for (i=3; i>=0; i--) printf("{%02x}", p1[i]);
    	printf(" CIRCLEX ");
   		for (i=3; i>=0; i--) printf("{%02x}", p2[i]);
   		printf(" = ");
    	for (i=3; i>=0; i--) printf("{%02x}", prod[i]);
    	printf("\n");
    } else if ( strcmp(option, "keyexpand") == 0) {
    	FILE *fTable = fileCheck(tablefile);
    	table_check(fTable);
    	keyexpansion(key);
    	int i;
    	for (i=0; i<44; i++) printf("w[%2d]: %08lx\n", i, keys[i]);
    } else {
    	inverse();
    	inverse_gf8(A_inv);
    	printf("Multiplicative inverse of {%02x}{%02x}{%02x}{%02x} is ", A_inv[3], A_inv[2], A_inv[1], A_inv[0]);
    	printf("{%02x}{%02x}{%02x}{%02x}\n", inv[3], inv[2], inv[1], inv[0]);
    }
	return(0);
}
